﻿using System;
using System.Collections.Generic;
using System.Text;
using ININ.IceLib;
using ININ.IceLib.Connection;
using ININ.IceLib.People;
using System.Collections.ObjectModel;

namespace ClientMemos
{
    class User
    {
        private Session icSession;
        private PeopleManager icPplMgr;

        public string name { get; set; }
        public string pass { get; set; }
        public string station { get; set; }
        public string server { get; set; }

        public User()
        {
            icSession = new Session();
            icPplMgr = PeopleManager.GetInstance(icSession);
        }

        public bool isConnected()
        {
            return (icSession.ConnectionState == ConnectionState.Up);
        }


        public void setStatus(StatusMessageDetails statusMessageDetails)
        {

            if (statusMessageDetails == null) return;

            var icUserStatusUpdate = new UserStatusUpdate(icPplMgr);
            icUserStatusUpdate.StatusMessageDetails = statusMessageDetails;
            icUserStatusUpdate.UpdateRequest();

        }

        public UserStatus getStatus()
        {
            var icStatusList = new UserStatusList(icPplMgr);
            return icStatusList.GetUserStatus(icSession.UserId);
        }


        public ReadOnlyCollection<StatusMessageDetails> getStatusList()
        {
            var statusList = new FilteredStatusMessageList(icPplMgr);

            statusList.StartWatching(new string[] { icSession.UserId });
            return statusList.GetList()[icSession.UserId];
        }

        public bool disconnect()
        {
            icSession.Disconnect();

            return (icSession.ConnectionState == ConnectionState.Down);
        }

        public Session connect()
        {
            var media = (SupportedMedia.Call | SupportedMedia.WorkItem);

            var icSessionSettings = new ININ.IceLib.Connection.SessionSettings();
            var icHostEndpoint = new ININ.IceLib.Connection.HostEndpoint(this.server);
            var icHostSettings = new ININ.IceLib.Connection.HostSettings(icHostEndpoint);
            var icStationSettings = new ININ.IceLib.Connection.WorkstationSettings(station, media);
            var icAuthSettings = new ININ.IceLib.Connection.ICAuthSettings(this.name, this.pass);

            try
            {
                icSession.Connect(icSessionSettings, icHostSettings, icAuthSettings, icStationSettings);
                return icSession;
            }
            catch (ININ.IceLib.IceLibException ie)
            {
                Console.WriteLine(ie.StackTrace);
                return null;
            }
        }

        public Session connect(StoredCredentials icCreds)
        {
            try
            {
                icSession.Connect(icCreds.SessionSettings, icCreds.HostSettings, icCreds.AuthSettings, icCreds.StationSettings);
                return icSession;
            }
            catch (ININ.IceLib.IceLibException ie)
            {
                Console.WriteLine(ie.StackTrace);
                return null;
            }
        }
    }
}
