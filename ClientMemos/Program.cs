﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

using ININ.IceLib.Connection;

using ININ.IceLib.Statistics;
using ININ.IceLib.Statistics.Alerts;


namespace ClientMemos
{
    static class Program
    {

        static void Main()
        {
            var icUser = new User();
            //
            //
            // IC Server Host Name:
            //
            icUser.server   =   "localhost";
            //
            //
            // IC User-Service Account with Verified Memo Access-Permissions:
            // (See "Supervisor" views in IC Business Manager application to verify memo access-authorization.)
            //
            icUser.name     =   "Administrator";
            icUser.pass     =   "12341234";
            icUser.station = "Administrator";
            //
            //
            // Memo Recipient(s), User(s) and Workgroup(s), inclusive "OR" (set logic):
            //
            var icMemoToUsers = new string[] { icUser.name, "User_1", "User_2", "User_3", "...etc..." };
            var icMemoToWorkgroups = new string[] { "Workgroup_1", "Workgroup_2", "Workgroup_3", "...etc..." };
            //
            //
            const string icMemo_MessageName  =   "Message Name";
            const string icMemo_MessageText  =   "Message Text";
            const string icMemoUrl = "http://www.inin.com";
            const string icMemo_SoundFile = "";
            //
            const bool icMemo_PopupType    =   true;
            const bool icMemo_CanExpire = false;
            //
            var icMemo_Expiration = new DateTime();
            var icMemo_Icon = SystemIcons.Warning;
            //
            //
            var icSession = icUser.connect();
            //
            if (icUser.isConnected())
            {
                var icMemo = Memo.CreateMemo(icSession, icMemo_MessageText, icMemo_MessageName, icMemoUrl, icMemo_PopupType, icMemo_Icon, icMemo_CanExpire, icMemo_Expiration, icMemo_SoundFile);
                var icStatMgr = StatisticsManager.GetInstance(icSession);
                var icMemoList = MemoList.CreateGeneralMemoList(icStatMgr);
                //
                // Subscribe and send memo to: users and workgroups (instantiated above):
                icMemoList.StartWatching();
                icMemoList.CommitMemo(icMemoToUsers, icMemoToWorkgroups, icMemo);
                //
                // Delete memo after time range (argument in milliseconds):
                System.Threading.Thread.Sleep(60000);
                icMemoList.DeleteMemo(icMemo.MessageId);
                icMemoList.StopWatching();
                //
                icUser.disconnect();
                //
            }
        }
    }
}
